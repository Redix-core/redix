#include <drivers/screen.h>
#include <drivers/string.h>
#include <drivers/memory.h>
#include <core/isr.h>
#include <core/idt.h>

void kmain() {
    clrscr();
    printf("Redix kernel mode started.\n");

    char* test = "RedixOSMemoryTest";
    char* dest;
    memcpy(test, dest, 18);

    printf("Memory test src: ");
    printf(test);
    printf(", dest: ");
    printf(dest);
    printf(", if the src string and dest string are different then the test failed.\n");

    isr_install();
    printf("Installed interrupts, Stage 1.\n");
}