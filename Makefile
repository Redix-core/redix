C_SOURCES = $(wildcard src/kernel/*.c src/drivers/*.c src/core/*.c)
HEADERS = $(wildcard src/kernel/*.h src/drivers/*.h src/core/*.h)
# Nice syntax for file extension replacement
OBJ = ${C_SOURCES:.c=.o src/core/interrupt.o}

# Change this if your cross-compiler is somewhere else
CC =  gcc -m32 -Isrc
GDB = gdb -m32
# -g: Use debugging symbols in gcc
CFLAGS = -g

default_target: Redix.img
# First rule is run by default
Redix.img: src/boot/bootsect.bin kernel.bin
	cat $^ > Redix.img

# '--oformat binary' deletes all symbols as a collateral, so we don't need
# to 'strip' them manually on this case
kernel.bin: src/boot/kernel_entry.o ${OBJ}
	ld -melf_i386 --ignore-unresolved-symbol _GLOBAL_OFFSET_TABLE_ -o $@ -Ttext 0x1000 $^ --oformat binary

# Used for debugging purposes
kernel.elf: src/boot/kernel_entry.o ${OBJ}
	ld -melf_i386 --ignore-unresolved-symbol _GLOBAL_OFFSET_TABLE_ -o $@ -Ttext 0x1000 $^ 

run: Redix.img
	qemu-system-i386 Redix.img

# Open the connection to qemu and load our kernel-object file with symbols
debug: Redix.img kernel.elf
	qemu-system-i386 -s -fda Redix.img &
	${GDB} -ex "target remote localhost:1234" -ex "symbol-file kernel.elf"

# Generic rules for wildcards
# To make an object, always compile from its .c
%.o: %.c ${HEADERS}
	${CC} ${CFLAGS} -ffreestanding -c $< -o $@

%.o: %.asm
	nasm $< -f elf -o $@

%.bin: %.asm
	nasm $< -f bin -o $@

clean:
	rm -rf *.bin *.dis *.o Redix.img *.elf
	rm -rf src/kernel/*.o src/boot/*.bin src/drivers/*.o src/boot/*.o src/core/*.o
